import pandas as pd
import os
from datetime import datetime


# get all zip folders
def get_zip_folders():
    location = r'D:\office_data\office_data\vaccine\leopard\OneDrive_1_2-8-2022\Private'
    folder_list = os.listdir(location)
    import zipfile

    destination_path = r'D:\office_data\office_data\vaccine\leopard\OneDrive_1_2-8-2022\private_unzip'

    for folder_location in folder_list:
        with zipfile.ZipFile(os.path.join(location, folder_location), 'r') as zip_ref:
            zip_ref.extractall(destination_path)

        process_file()


def process_file():
    path = r'D:\office_data\office_data\vaccine\leopard\OneDrive_1_2-8-2022\private_unzip'
    product = pd.read_csv(os.path.join(path, 'PRODUCT.txt'), header=None, sep=';')
    data = pd.read_csv(os.path.join(path, 'DATA.txt'), header=None, sep=';')
    geo = pd.read_csv(os.path.join(path, 'GEOGRAPHY.txt'), header=None, sep=';')

    product = product[[0, 1, 2, 5]].copy()
    product.columns = ['Product Code', 'Market Code', 'Market Name', 'Product Name']

    data = data[[0, 1, 2, 3, 4, 7]].copy()
    data.columns = ['Brick Code', 'Market Code', 'Product Code', 'Time Period', 'UN', 'LC']

    geo = geo[[0, 5]].copy()
    geo.columns = ['Brick Code', 'Brick Name']

    data['LC'] = data['LC'].astype(str)
    data['LC'] = data['LC'].str.replace(',', '.')
    data['LC'] = data['LC'].astype(float)

    data['Time Period'] = data['Time Period'].str[1:5]
    data['Time Period'] = [datetime.strptime(date_value, '%y%m') for date_value in data['Time Period']]
    data['Time Period'] = [datetime.strftime(date_value, '%Y%m') for date_value in data['Time Period']]

    product.drop_duplicates(subset=['Product Code', 'Market Code'], inplace=True)
    geo.drop_duplicates(subset=['Brick Code'], inplace=True)

    x1 = pd.merge(data, product, on=['Product Code', 'Market Code'], how='left')
    x2 = pd.merge(x1, geo, on=['Brick Code'], how='left')

    date_list = x2['Time Period'].unique()
    date_list.sort()
    date_list = '_'.join(date_list)
    date_list = date_list+'.csv'
    x2.to_csv(os.path.join(r'D:\office_data\office_data\vaccine\leopard\output\private', date_list), index=False)
    print()


def merge_output_all():
    location = r'D:\office_data\office_data\vaccine\leopard\output\private\all'
    file_list = os.listdir(location)

    final_df = pd.DataFrame()
    for file in file_list:
        data = pd.read_csv(os.path.join(location, file))
        # remove latest year
        final_df = final_df.append(data, ignore_index=True)

    final_df.to_csv(os.path.join(location, 'final.csv'), index=False)

def merge_output_previous():
    location = r'D:\office_data\office_data\vaccine\leopard\output\private\previous'
    file_list = os.listdir(location)

    final_df = pd.DataFrame()
    for file in file_list:
        data = pd.read_csv(os.path.join(location, file))
        # remove latest year
        date_list = list(data['Time Period'].unique())
        date_list.sort()
        data = data[data['Time Period'] == date_list[0]].copy()
        final_df = final_df.append(data, ignore_index=True)

    final_df.to_csv(os.path.join(location, 'final.csv'), index=False)


def merge_previous_and_all_output():
    location = r'D:\office_data\office_data\vaccine\leopard\output\private\previous'
    previous = pd.read_csv(os.path.join(location, 'final_previous.csv'))

    location = r'D:\office_data\office_data\vaccine\leopard\output\private\all'
    all = pd.read_csv(os.path.join(location, 'final_all.csv'))

    df_final = all.append(previous)
    df_final['Brick Code'] = df_final['Brick Code'].str.replace('B', '0')
    df_final.to_excel(r'D:\office_data\office_data\vaccine\leopard\output/out.xlsx', index=False)
    print()


if __name__ == '__main__':
    # get_zip_folders()
    # merge_output_all()
    merge_previous_and_all_output()
